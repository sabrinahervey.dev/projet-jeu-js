// Affiche la popup en rendant visible l'élément ayant la classe "popupBackground"
function afficherPopup() {
    let popupBackground = document.querySelector(".popupBackground")
    // La popup est masquée par défaut donc ajouter la classe "active"
    //pour la rendre visible. 
    popupBackground.classList.add("active")
}

// Cache la popup en rendant invisible l'élément ayant la classe "popupBackground
function cacherPopup() {
    let popupBackground = document.querySelector(".popupBackground")
    // La popup est masquée par défaut donc supprimer la classe "active"
    //pour rétablir cet affichage par défaut. 
    popupBackground.classList.remove("active")
}
// Initialise les écouteurs d'événements pour la popup
function initAddEventListenerPopup() {
    // On écoute le click sur le bouton "partager"
    btnPartage = document.querySelector(".zonePartage button")
    let popupBackground = document.querySelector(".popupBackground")
    btnPartage.addEventListener("click", () => {
        // Quand on a cliqué sur le bouton partagé, on affiche la popup
        afficherPopup()
    })

    // On écoute le click sur la div "popupBackground"
    popupBackground.addEventListener("click", (event) => {
        // Si on a cliqué précisément sur la popupBackground 
        if (event.target === popupBackground) {
            // Alors on cache la popup
            cacherPopup()
        }
    })
}
// Appel de la fonction pour initialiser les écouteurs d'événements de la popup
 initAddEventListenerPopup();