//function pour afficher le résultat du score et du nombre de mots
function afficherResultat(score, nbMotsProposes) {
    let spanScore = document.querySelector(".zoneScore span");

    let affichageScore = `${score} / ${nbMotsProposes}`
    spanScore.innerText = affichageScore;
}
   
//function qui affiche une proposition dans la zone de proposition
function afficherProposition(proposition) {
    let zoneProposition = document.querySelector(".zoneProposition")
    zoneProposition.innerText = proposition
}
/**
* Cette fonction génère un lien mailto avec le nom du joueur, son score et l'adresse email fournie.
* Lorsque le lien est cliqué, il ouvre le client de messagerie par défaut de l'utilisateur avec un nouveau message pré-rempli.
* @param {string} nom : le nom du joueur
* @param {array[string]} listePropositions 
* @return {number} : le score de l'utilisateur
*/
function afficherEmail(nom, email, score) {
    // Construire le lien mailto avec le nom, le score et l'email
    let mailto = `mailto:${email}?subject=Partage du score Azertype&body=Salut, je suis ${nom} et je viens de réaliser le score ${score} sur le site d'Azertype !`
    // Rediriger l'utilisateur vers le lien mailto
    location.href = mailto
}
//fonction qui qui valide le nom du joueur
function validerNom(nom) {
    if (nom.length >= 2) {
        return true
    }
    return false
}
//fonction pour valider l'email
function validerEmail(email) {
    let emailRegExp = new RegExp("[a-z0-9._-]+@[a-z0-9._-]+\\.[a-z0-9._-]+")
    if (emailRegExp.test(email)) {
        return true
    }
    return false
}
/**
* Cette fonction lance le jeu. 
* Elle demande à l'utilisateur de choisir entre "mots" et "phrases" et lance la boucle de jeu correspondante
*/
function lancerJeu() {
  // Initialisations des variables
  let score = 0;
  let i = 0;
  let listeProposition = listeMots;

  //Récupération des éléments du DOM
  let btnValiderMot = document.getElementById("btnValiderMot");
  let inputEcriture = document.getElementById("inputEcriture");
  //Affichage de la première proposition
  afficherProposition(listeProposition[i]);
  //gestion du clic sur le bouton "valider"
  btnValiderMot.addEventListener("click", () => {
    console.log(inputEcriture.value);
    //vérification de la réponse
    if (inputEcriture.value === listeProposition[i]) {
      score++;//incrémentation du score si la réponse est correcte
    }
    i++;//passsage à la proposition suivante
    afficherResultat(score, i);
    inputEcriture.value = "";
    //vérification si la liste de proposition est terminée
    if (listeProposition[i] === undefined) {
      afficherProposition("le jeu est terminé");
      //désactivation du bouton "valider"
      btnValiderMot.disabled = true;
    } else {
      //affichage de la proposition suivante
      afficherProposition(listeProposition[i]);
    }
  });

  // Gestion de l'événement change sur les boutons radios.
  let listeBtnRadio = document.querySelectorAll(".optionSource input");
  for (let index = 0; index < listeBtnRadio.length; index++) {
    listeBtnRadio[index].addEventListener("change", (event) => {
      // Si c'est le premier élément qui a été modifié, alors nous voulons
      // jouer avec la listeMots.
      if (event.target.value === "1") {
        listeProposition = listeMots;
      } else {
        // Sinon nous voulons jouer avec la liste des phrases
        listeProposition = listePhrases;
      }
      // Et on modifie l'affichage en direct.
      afficherProposition(listeProposition[i]);
    });
  }
  
  // Gestion de l'envoi du formulaire
  let form = document.querySelector("form");
  // On empêche le comportement par défaut du formulaire.
  form.addEventListener("submit", (event) => {
    event.preventDefault();

    // On récupère les valeurs des champs nom et email.
    let nom = document.getElementById("nom").value;
    let email = document.getElementById("email").value;
    // On vérifie que le nom et l'email sont valides.
    if (validerNom(nom) && validerEmail(email)) {
        // On crée le lien mailto 
        let scoreEmail = `${score} / ${i}`;
        afficherEmail(nom, email, score);
    } else {
        console.log("erreur")
    }
    //affichage du résultat final  
    afficherResultat(score, i);
  });
}