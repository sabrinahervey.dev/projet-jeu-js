# AZERTYPE APPLICATION 

Azertype est un jeu où vous devez saisir des mots ou des phrases selon la catégorie choisie. Testez vos compétences en dactylographie et voyez jusqu'où vous pouvez aller !

Ce mini projet a été fait en suivant le cours d'open classroom sur apprendre à programmer en java Script 
voici le lien du cours https://openclassrooms.com/fr/courses/7696886-apprenez-a-programmer-avec-javascript

* Fonctions principales

afficherResultat(score, nbMotsProposes)
Cette fonction affiche dans la console le score de l'utilisateur.

score: le score de l'utilisateur.
nbMotsProposes: le nombre de mots proposés à l'utilisateur.

* language/tech utilisées

il est écrit en Java Script.

* lien pour cloner le repo

git@gitlab.com:sabrinahervey.dev/projet-jeu-js.git